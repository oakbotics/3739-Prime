package ca.oakbotics.robot.auto;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoTurn extends Command {
	
	private final static double kP = 0.044, kI = 0, kD = 0.11;
	
	private double rotate;
	
	private final PIDController gyroPID;

    public AutoTurn(double degree) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.driveBase);
    	
    	gyroPID = new PIDController(kP, kI, kD, Robot.driveBase.getGyro(), new PIDOutput(){

			@Override
			public void pidWrite(double output) {
				rotate = output;
			}
		});
    	
    	gyroPID.setSetpoint(degree);
    	gyroPID.setAbsoluteTolerance(2.5);
    	gyroPID.setOutputRange(-0.75, 0.75);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	gyroPID.reset();
    	gyroPID.enable();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	Robot.driveBase.arcadeDrive(0, -rotate);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return gyroPID.onTarget();
    }

    // Called once after isFinished returns true
    protected void end() {
    	gyroPID.disable();
    	Robot.driveBase.stop();
    	gyroPID.reset();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    	gyroPID.disable();
    	Robot.driveBase.stop();
    	gyroPID.reset();
    }
}