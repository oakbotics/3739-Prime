package ca.oakbotics.robot.auto;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

public class AutoDrive extends Command {
	
	private PIDController gyroPID, encoderPID;
	
	private final static double kPE = 0.025, kIE = 0.0, kDE = 0.08;
	private final static double kPG = 0.01, kIG = 0, kDG = 0.004;
	
	private double speed;
	private double rotate;
	
	public AutoDrive(double distance, double maxSpeed) {
		requires(Robot.driveBase);

		encoderPID = new PIDController(kPE, kIE, kDE, new PIDSource() {
			PIDSourceType m_sourceType = PIDSourceType.kDisplacement;

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
				m_sourceType = pidSource;
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return m_sourceType;
			}

			@Override
			public double pidGet() {
				double distance = Robot.driveBase.getAvgEncoderDistance();
				return distance;
			}
		}, new PIDOutput() {
			@Override
			public void pidWrite(double pidSpeed) {
				speed = pidSpeed;
			}
		});
		
		gyroPID = new PIDController(kPG, kIG, kDG, Robot.driveBase.getGyro(), new PIDOutput() {
			@Override
			public void pidWrite(double PIDRotate) {
				rotate = PIDRotate;
			}
		});
		
		gyroPID.setAbsoluteTolerance(1.0);
    	gyroPID.setOutputRange(-0.4, 0.4);
    	    	
    	encoderPID.setOutputRange(-maxSpeed, maxSpeed);
    	
    	encoderPID.setSetpoint(distance);
    	encoderPID.setAbsoluteTolerance(4.0);
    	encoderPID.setName("DrivePID", "Motors PID");
    	LiveWindow.add(encoderPID);
	}
	
	@Override
	protected void initialize() {
    	Robot.driveBase.resetEncoders();
    	Robot.driveBase.resetGyro();
    	gyroPID.reset();
    	encoderPID.reset();
    	gyroPID.enable();
    	encoderPID.enable();
    	
    	gyroPID.setSetpoint(0);
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
     	Robot.driveBase.arcadeDrive(speed, -rotate);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return encoderPID.onTarget();
    }

    // Called once after isFinished returns true
    protected void end() {
    	gyroPID.disable();
    	encoderPID.disable();
    	Robot.driveBase.stop();
    	gyroPID.reset();
    	encoderPID.reset();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    	gyroPID.disable();
    	encoderPID.disable();
    	Robot.driveBase.stop();
    	gyroPID.reset();
    	encoderPID.reset();
    }

}
	

