package ca.oakbotics.robot;

import edu.wpi.first.wpilibj.buttons.JoystickButton;
import ca.oakbotics.robot.commands.IntakeReverse;
import ca.oakbotics.robot.commands.TeleTurn;
import ca.oakbotics.robot.triggers.HatLeft;
import ca.oakbotics.robot.triggers.HatRight;
import ca.oakbotics.robot.commands.ArmLower;
import ca.oakbotics.robot.commands.ArmRaise;
import ca.oakbotics.robot.commands.ClimbDown;
import ca.oakbotics.robot.commands.ClimbUp;
import ca.oakbotics.robot.commands.DriveToRange;
import edu.wpi.first.wpilibj.XboxController;

/**
 * This class is the glue that binds the controls on the physical operator interface to the commands
 * and command groups that allow control of the robot.
 */
public class OI {

	private XboxController driveController, operatorController;

	public OI() {
		driveController = new XboxController(Config.DRIVE_XBOX_PORT);
		operatorController = new XboxController(Config.OP_XBOX_PORT);
		
		new JoystickButton(operatorController, 4).toggleWhenPressed(new IntakeReverse());
		new JoystickButton(operatorController, 2).whenPressed(new ArmRaise());
		new JoystickButton(operatorController, 3).whenPressed(new ArmLower());
		new JoystickButton(operatorController, 1 ).toggleWhenPressed(new DriveToRange(15, 1.0));
				
		new JoystickButton(operatorController, 5).whenPressed(new ClimbUp());
		new JoystickButton(operatorController, 6).whenPressed(new ClimbDown());
		
		new JoystickButton(driveController, 1).toggleWhenPressed(new DriveToRange(12, 0.5));
		
		new HatLeft(driveController).whenActive(new TeleTurn(-90));
		new HatRight(driveController).whenActive(new TeleTurn(90));
	}
	
	public XboxController getDrive() {
		return driveController;
	}
	
	public XboxController getOperator() {
		return operatorController;
	}
}