package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * This command allows the robot to be controlled by an Xbox controller. The
 * right trigger controls forward motion, the left trigger controls reverse, and
 * the left stick controls turning. If 'X' is held, the left and right trigger
 * controls or swapped (the right controls reverse).
 */
public class TeleDrive extends Command {

	public TeleDrive() {
		// Use requires() here to declare subsystem dependencies.
		requires(Robot.driveBase);
	}

	// Called just before this Command runs the first time.
	@Override
	protected void initialize() {
	}

	// Called repeatedly when this Command is scheduled to run.
	@Override
	protected void execute() {
		double moveValue = Robot.oi.getDrive().getY(Hand.kLeft);
		double rotateValue = Robot.oi.getDrive().getX(Hand.kRight);
		Robot.driveBase.arcadeDrive(-moveValue, -rotateValue, true);
	}

	@Override
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	@Override
	protected void end() {
		// Stops power to the drive train when this commend is finished
		Robot.driveBase.stop();
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	@Override
	protected void interrupted() {
		// Stops power to the drive train when this commend is finished
		Robot.driveBase.stop();
	}
}
