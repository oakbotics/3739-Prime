package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.auto.AutoDrive;
import ca.oakbotics.robot.auto.AutoTurn;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 *
 */
public class AutoRightFrontSwitch extends CommandGroup {

    public AutoRightFrontSwitch() {
        // Add Commands here:
        // e.g. addSequential(new Command1());
        //      addSequential(new Command2());
        // these will run in order.

        // To run multiple commands at the same time,
        // use addParallel()
        // e.g. addParallel(new Command1());
        //      addSequential(new Command2());
        // Command1 and Command2 will run in parallel.

        // A command group will require all of the subsystems that each member
        // would require.
        // e.g. if Command1 requires chassis, and Command2 requires arm,
        // a CommandGroup containing them would require both the chassis and the
        // arm.
    	addSequential(new Delay(8000));
    	addSequential(new AutoDrive(36, 0.7), 0.7);
    	if (DriverStation.getInstance().getGameSpecificMessage().startsWith("L")) {
    		addSequential(new AutoTurn(-90), 0.9);
    		addSequential(new AutoDrive(100, 0.8), 1.2);
    		addSequential(new AutoTurn(90), 0.9);
    		addSequential(new AutoDrive(60, 0.7), 0.8);
    		addSequential(new AutoTurn(90), 0.9);
    		addSequential(new DriveToRange(3, 0.35), 1.5);
    		addSequential(new IntakeReverse(), 0.5);
    		addSequential(new AutoDrive(-40, 0.7));
    	}
    	else {
    		addSequential(new AutoTurn(90), 0.9);
    		addSequential(new AutoDrive(36, 0.8), 1.2);
    		addSequential(new AutoTurn(-90), 0.9);
    		addSequential(new AutoDrive(60, 0.7), 0.8);
    		addSequential(new AutoTurn(-90), 0.9);
    		addSequential(new DriveToRange(3, 0.35), 1.5);
    		addSequential(new IntakeReverse(), 0.5);
    		addSequential(new AutoDrive(-40, 0.7));
    	}
    }
}
