package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class DriveToRange extends Command {

	private PIDController ultraPID;
	private PIDController gyroPID;
	
	private final static double kPU = 0.11, kIU = 0.00, kDU = 0.3;
	private final static double kPG = 0.035, kIG = 0, kDG = 0.12;
	
	private double speed, rotate;
	
    public DriveToRange(double range, double maxspeed) {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.driveBase);
    	
    	ultraPID = new PIDController(kPU, kIU, kDU, Robot.driveBase.getUltra(), new PIDOutput() {
    		
			@Override
			public void pidWrite(double output) {
				speed = output;
			}
		});
    	
    	gyroPID = new PIDController(kPG, kIG, kDG, Robot.driveBase.getGyro(), new PIDOutput() {
    		
			@Override
			public void pidWrite(double output) {
				rotate = output;
			}
		});
    	
    	ultraPID.setAbsoluteTolerance(1.0);
    	ultraPID.setSetpoint(range);
    	ultraPID.setOutputRange(-maxspeed, maxspeed);
    	
    	gyroPID.setAbsoluteTolerance(1.5);
    	gyroPID.setOutputRange(-0.4, 0.4);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	Robot.driveBase.getUltra().setAutomaticMode(true);
    	Robot.driveBase.getUltra().setEnabled(true);
    	
    	gyroPID.setSetpoint(Robot.driveBase.getAngle());
    	ultraPID.enable();
    	gyroPID.enable();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	Robot.driveBase.arcadeDrive(-speed, -rotate);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return ultraPID.onTarget();
    }

    // Called once after isFinished returns true
    protected void end() {
    	ultraPID.disable();
    	gyroPID.disable();
    	Robot.driveBase.stop();
    	ultraPID.reset();
    	gyroPID.reset();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    	ultraPID.disable();
    	gyroPID.disable();
    	Robot.driveBase.stop();
    	ultraPID.reset();
    	gyroPID.reset();
    }
}
