package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import ca.oakbotics.robot.auto.AutoDrive;
import ca.oakbotics.robot.auto.AutoTurn;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 *
 */
public class AutoCenter extends CommandGroup {

    public AutoCenter() {
        // Add Commands here:
        // e.g. addSequential(new Command1());
        //      addSequential(new Command2());
        // these will run in order.

        // To run multiple commands at the same time,
        // use addParallel()
        // e.g. addParallel(new Command1());
        //      addSequential(new Command2());
        // Command1 and Command2 will run in parallel.

        // A command group will require all of the subsystems that each member
        // would require.
        // e.g. if Command1 requires chassis, and Command2 requires arm,
        // a CommandGroup containing them would require both the chassis and the
        // arm.
    	Robot.driveBase.resetGyro();
    	Robot.driveBase.resetEncoders();
    	
    	addSequential(new AutoDrive(30, 1.0));
    	String gameMessage = Robot.gameMessage;
    	boolean timeOut = false;
    	
    	double startTime = System.currentTimeMillis();
		while (gameMessage.length() != 3 && !timeOut) {
			gameMessage = DriverStation.getInstance().getGameSpecificMessage();

			double currentTime = System.currentTimeMillis();
			if ((currentTime - startTime) >= 10000) {
				timeOut = true;
				break;
			}
		}
    	System.out.println("GameMessage NOT NULL: " + gameMessage);
    	
    	if (timeOut) {
    		System.out.println("5s remaining in auto and gameMessage not resolved, defaulting to baseline auto");
    		addSequential(new AutoTurn(-90), 1.0);
    		addSequential(new AutoDrive(100, 1.0));
    		addSequential(new AutoTurn(90), 1.0);
    		addSequential(new AutoDrive(80, 1.0));
    	}
    	else {
    		double turnConstant = 1;
    		if (gameMessage.startsWith("L"))
    			turnConstant = -1;
    		addSequential(new AutoTurn(turnConstant * 45));
    		addSequential(new AutoDrive(80, 1.0));
    		addSequential(new AutoTurn(turnConstant * -45));
    		double initial = Robot.driveBase.getUltra().getRangeInches();
    		addSequential(new DriveToRange(3, 1.0));
    		addSequential(new IntakeReverse(), 0.5);
    		addSequential(new AutoDrive(-12, 1.0));
    		addSequential(new AutoTurn(turnConstant * 45));
    		addSequential(new AutoDrive(-80, 1.0));
    		addSequential(new AutoTurn(turnConstant * -45));
    		initial = Robot.driveBase.getUltra().getRangeInches();
    		addSequential(new DriveToRange(5, 0.8));
    		addParallel(new IntakeForwards(), 1.0);
    		addSequential(new AutoDrive(4, 0.5), 1.0);
    		addSequential(new AutoDrive(-48, 1.0));
    		addSequential(new AutoTurn(turnConstant * 45));
    		addSequential(new AutoDrive(80, 1.0));
    		addSequential(new AutoTurn(turnConstant * -45));
    		addSequential(new DriveToRange(3, 1.0));
    		addSequential(new IntakeReverse(), 0.5);
    		addSequential(new AutoDrive(-40, 1.0));
    		
//	    	if (gameMessage.startsWith("L")) {
//	    		System.out.println("Front Switch on LEFT");
//		    	addSequential(new AutoTurn(-90), 1.0);
//		    	addSequential(new AutoDrive(66, 1.0));
//		    	addSequential(new AutoTurn(90), 1.0);
//			}
//			else if (gameMessage.startsWith("R")) {
//	    		System.out.println("Front Switch on RIGHT");
//	    		addSequential(new AutoTurn(90), 1.0);
//		    	addSequential(new AutoDrive(66, 1.0));
//		    	addSequential(new AutoTurn(-90), 1.0);
//	    	}
//	    	
//	    	addSequential(new DriveToRange(3.5, 1.0));
//	  		addSequential(new IntakeReverse(), 1.0);
//	  		//addParallel(new ArmLower());
//	    	addSequential(new AutoDrive(-60, 1.0));
//	    	
//	    	if (gameMessage.startsWith("L")) {
//	    		addSequential(new AutoTurn(90), 1.0);
//	    		addSequential(new AutoDrive(66, 1.0));
//	    		addSequential(new AutoTurn(-90), 1.0);
//	    		addParallel(new IntakeForwards(), 1.2);
//	    		addSequential(new DriveToRange(2, 0.3));
//	    		addSequential(new AutoDrive(4, 0.6));
//	    		addSequential(new AutoDrive(-40, 1.0));
//	    		addSequential(new AutoTurn(-90), 1.0);
//	    		addSequential(new AutoDrive(66, 1.0));
//	    		addSequential(new AutoTurn(90), 1.0);
//	    		addSequential(new DriveToRange(3.5, 1.0));
//	    		addSequential(new IntakeReverse(), 0.5);
//	    		addSequential(new AutoDrive(-48, 1.0));
//	    	}
//	    	
//	    	else if (gameMessage.startsWith("R")) {
//	    		addSequential(new AutoTurn(-90), 1.0);
//	    		addSequential(new AutoDrive(66, 1.0));
//	    		addSequential(new AutoTurn(90), 1.0);
//	    		addParallel(new IntakeForwards(), 1.5);
//	    		addSequential(new DriveToRange(4, 0.3));
//	    		addSequential(new AutoDrive(4, 0.6));
//	    		addSequential(new AutoDrive(-40, 1.0));
//	    		addSequential(new AutoTurn(90), 1.0);
//	    		addSequential(new AutoDrive(66, 1.0));
//	    		addSequential(new AutoTurn(-90), 1.0);
//	    		addSequential(new DriveToRange(3.5, 1.0));
//	    		addSequential(new IntakeReverse(), 0.5);
//	    		addSequential(new AutoDrive(-48, 1.0));
//	    	}
    	}
    }
}
