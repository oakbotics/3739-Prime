package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.Robot;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class IntakeOperator extends Command {

	
	private int counter;
	
    public IntakeOperator() {
        requires(Robot.intake);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	counter = 0;
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	
//    	double intakeSpeed;
//    	
//	   	intakeSpeed = Math.abs(Robot.oi.getOperator().getTriggerAxis(Hand.kRight));
    	
    	double lSpeed = Math.abs(Robot.oi.getOperator().getTriggerAxis(Hand.kLeft));
    	double rSpeed = Math.abs(Robot.oi.getOperator().getTriggerAxis(Hand.kRight));
    	
    	double currentAverage = (Robot.pdp.getCurrent(13) + Robot.pdp.getCurrent(2)) / 2;
    	
    	if (currentAverage > 10 && counter > 10) {
			Robot.oi.getOperator().setRumble(RumbleType.kLeftRumble, currentAverage / 10.0);
    		Robot.oi.getOperator().setRumble(RumbleType.kRightRumble, currentAverage / 10.0);

    	}
	    	
    	else {
			Robot.oi.getOperator().setRumble(RumbleType.kLeftRumble, 0);
    		Robot.oi.getOperator().setRumble(RumbleType.kRightRumble, 0);
    		if (lSpeed == 0 && rSpeed == 0) counter = 0;
    		//if (intakeSpeed == 0) counter = 0;
    		else counter++;
    	}
	    //Robot.intake.setIntakeSpeed(intakeSpeed);
    	Robot.intake.setIntakeSpeed(lSpeed, rSpeed);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
    	Robot.intake.stop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    	Robot.intake.stop();
    }
}
