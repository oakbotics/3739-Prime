package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import ca.oakbotics.robot.auto.AutoDrive;
import ca.oakbotics.robot.auto.AutoTurn;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 *
 */
public class AutoLeft extends CommandGroup {
		
    public AutoLeft() {
        // Add Commands here:
        // e.g. addSequential(new Command1());
        //      addSequential(new Command2());
        // these will run in order.

        // To run multiple commands at the same time,
        // use addParallel()
        // e.g. addParallel(new Command1());
        //      addSequential(new Command2());
        // Command1 and Command2 will run in parallel.

        // A command group will require all of the subsystems that each member
        // would require.
        // e.g. if Command1 requires chassis, and Command2 requires arm,
        // a CommandGroup containing them would require both the chassis and the
        // arm.
    	Robot.driveBase.resetGyro();
    	Robot.driveBase.resetEncoders();
    	
    	addSequential(new AutoDrive(30, 0.4), 1.0);
    	String gameMessage = Robot.gameMessage;
    	boolean timeOut = false;
    	
    	while (gameMessage.length() != 3) {
    		gameMessage = DriverStation.getInstance().getGameSpecificMessage();    		
    		timeOut = Timer.getMatchTime() <= 5;
    	}
    	System.out.println("GameMessage NOT NULL: " + gameMessage);
    	
//    	if (timeOut) {
//    		System.out.println("5s remaining in auto and gameMessage not resolved, defaulting to baseline auto");
//    		addSequential(new AutoTurn(-90), 1.0);
//    		addSequential(new AutoDrive(32, 0.5));
//    		addSequential(new AutoTurn(90), 1.0);
//    		addSequential(new AutoDrive(76, 0.6));
//    	}
//    	else {
    		if (gameMessage.startsWith("L")) {
        		System.out.println("Front Switch on LEFT");
        		addSequential(new AutoTurn(-90), 1.0);
    	    	addSequential(new AutoDrive(40, 0.5), 1.0);
    	    	addSequential(new AutoTurn(90), 1.0);
    	    	addSequential(new AutoDrive(90, 0.5), 1.6);
    	    	addSequential(new AutoTurn(90), 1.0);
    	    	addSequential(new AutoDrive(18, 0.5), 1.0);	
    		}
    		else if (gameMessage.startsWith("R")) {
        		System.out.println("Front Switch on RIGHT");
        		addSequential(new AutoTurn(90), 1.0);
        		addSequential(new AutoDrive(120, 0.5), 2.0);
        		addSequential(new AutoTurn(-90), 1.0);
        		addSequential(new AutoDrive(90, 0.5), 1.6);
        		addSequential(new AutoTurn(-90), 0.5);
        		addSequential(new AutoDrive(18, 0.5), 1.0);
        	}
    		addSequential(new DriveToRange(4, 0.3), 1.0);
	    	addSequential(new IntakeReverse(), 0.5);
	    	//addParallel(new ArmLower());
	    	addSequential(new AutoDrive(-48, 0.4));
//    	}
    }
}
