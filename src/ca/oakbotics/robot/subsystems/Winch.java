package ca.oakbotics.robot.subsystems;

import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.commands.WinchDualIn;
import ca.oakbotics.robot.commands.WinchIn;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

/**
 *
 */
public class Winch extends Subsystem {

    // Put methods for controlling this subsystem
    // here. Call these from Commands.
	
	private Spark mtr1, mtr2;
	private SpeedControllerGroup motors;
	private DoubleSolenoid climbArm;
	
	public Winch() {
		mtr1 = new Spark(Config.WINCH_MTR_1);
		mtr2 = new Spark(Config.WINCH_MTR_2);
		mtr1.setInverted(Config.WINCH_1_INVERTED);
		mtr2.setInverted(Config.WINCH_2_INVERTED);
		
		motors = new SpeedControllerGroup(mtr1, mtr2);
		climbArm = new DoubleSolenoid(3, 2);
		
		mtr1.setName("Winch", "Motor 1");
		mtr2.setName("Winch", "Motor 2");
		LiveWindow.add(mtr1);
		LiveWindow.add(mtr2);
	}

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	setDefaultCommand(new WinchIn());
    }
    
    public void setSpeed(double speed) {
    	motors.set(speed);
    }
    
    public void setDualSpeed(double speed1, double speed2) {
    	mtr1.setSpeed(speed1);
    	mtr2.setSpeed(speed2);
    }
    
    public void armUp() {
    	climbArm.set(Value.kForward);
    }
    
    public void armDown() {
    	climbArm.set(Value.kReverse);
    }
}

