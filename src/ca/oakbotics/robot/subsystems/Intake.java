package ca.oakbotics.robot.subsystems;

import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.commands.IntakeOperator;
import ca.oakbotics.robot.commands.IntakeOperator;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

/**
 *
 */
public class Intake extends Subsystem {

    // Put methods for controlling this subsystem
    // here. Call these from Commands.
	private Spark lMotor;
	private Spark rMotor;
	
	private DoubleSolenoid armDS_L, armDS_R;
	
	private SpeedControllerGroup intakeMotors;
	
	public Intake() {
		armDS_L = new DoubleSolenoid(Config.ARM_DS_A, Config.ARM_DS_B);
		armDS_R = new DoubleSolenoid(4, 5);
		
		lMotor = new Spark(Config.L_INTAKE_MTR);
		rMotor = new Spark(Config.R_INTAKE_MTR);
		
		lMotor.setInverted(Config.L_INTAKE_INVRTD);
		rMotor.setInverted(Config.R_INTAKE_INVRTD);
		
		intakeMotors = new SpeedControllerGroup(lMotor, rMotor);
		
		lMotor.setName("Intake", "Left Motor");
		rMotor.setName("Intake", "Right Motor");
		intakeMotors.setName("Intake", "Motors");
		armDS_L.setName("Intake", "Arm DS");
		
		LiveWindow.add(intakeMotors);
		LiveWindow.add(lMotor);
		LiveWindow.add(rMotor);
		LiveWindow.add(armDS_L);
		LiveWindow.add(armDS_R);
	}
	

    public void initDefaultCommand() {
        setDefaultCommand(new IntakeOperator());
    }
    
    public void setIntakeSpeed(double speed) {
    	intakeMotors.set(speed);
    }
    
    public void setIntakeSpeed(double lSpeed, double rSpeed) {
    	lMotor.set(lSpeed);
    	rMotor.set(rSpeed);
    }
    
    public void stop() {
    	intakeMotors.stopMotor();
    }
    
    public void raiseArm() {
    	armDS_L.set(DoubleSolenoid.Value.kForward);
    	armDS_R.set(DoubleSolenoid.Value.kForward);
    }
    
    public void lowerArm() {
    	armDS_L.set(DoubleSolenoid.Value.kReverse);
    	armDS_R.set(DoubleSolenoid.Value.kReverse);
    }
}

