package ca.oakbotics.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import ca.oakbotics.robot.Config;
import ca.oakbotics.robot.Robot;
import ca.oakbotics.robot.commands.TeleDrive;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Ultrasonic;
import edu.wpi.first.wpilibj.Ultrasonic.Unit;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.XboxController;

/**
 * The DriveTrain subsystem currently includes the two pairs of drive motors (controllers).
 * Robot goes 10 ft/s
 * @author Ben
 */
public class DriveBase extends Subsystem {

	private Victor lMotorA;
	private Victor lMotorB;
	private Victor rMotorA;
	private Victor rMotorB;
	
	private Encoder lEncoder;
	private Encoder rEncoder;
	
	private ADXRS450_Gyro gyro;
	
	private Ultrasonic ultra;
	
	private SpeedControllerGroup lMotors;
	private SpeedControllerGroup rMotors;
	
	private DifferentialDrive drive;

	public DriveBase() {
		// Speed controller definitions.
		lMotorA = new Victor(Config.L_MTR_A);
		lMotorB = new Victor(Config.L_MTR_B);
		rMotorA = new Victor(Config.R_MTR_A);
		rMotorB = new Victor(Config.R_MTR_B);

		// Setting the motors to inverted (due to the gear boxes).
		lMotorA.setInverted(Config.L_MTRS_INVRTD);
		lMotorB.setInverted(Config.L_MTRS_INVRTD);
		rMotorA.setInverted(Config.R_MTRS_INVRTD);
		rMotorB.setInverted(Config.R_MTRS_INVRTD);
		
		lEncoder = new Encoder(Config.L_ENCODER_A, Config.L_ENCODER_B, Config.L_ENCODER_INVRTD, EncodingType.k4X);
		rEncoder = new Encoder(Config.R_ENCODER_A, Config.R_ENCODER_B, Config.R_ENCODER_INVRTD, EncodingType.k4X);
		
		lEncoder.setDistancePerPulse(Config.ENC_CM_PER_TICK);
		rEncoder.setDistancePerPulse(Config.ENC_CM_PER_TICK);
		lEncoder.setMaxPeriod(0.1);
		rEncoder.setMaxPeriod(0.1);
		lEncoder.setMinRate(10);
		rEncoder.setMinRate(10);
		lEncoder.setSamplesToAverage(7);
		rEncoder.setSamplesToAverage(7);
		
		gyro = new ADXRS450_Gyro();
		gyro.calibrate();
				
		// Setting SpeedController Groups
		lMotors = new SpeedControllerGroup(lMotorA, lMotorB);
		rMotors = new SpeedControllerGroup(rMotorA, rMotorB);
		
		ultra = new Ultrasonic(4,5, Unit.kInches);
		
		lMotors.setName("Drive", "Left Motors");
		rMotors.setName("Drive", "Right Motors");

		lEncoder.setName("Drive", "Left Encoder");
		rEncoder.setName("Drive", "Right Encoder");
		
		gyro.setName("Drive", "Gyro");
		
		ultra.setName("Drive", "Ultrasonic");
		
		LiveWindow.add(lMotors);
		LiveWindow.add(rMotors);
		LiveWindow.add(lEncoder);
		LiveWindow.add(rEncoder);
		LiveWindow.add(gyro);
		LiveWindow.add(ultra);
		
		ultra.setAutomaticMode(true);
		ultra.setEnabled(true);
		
		// Defining the drive system.
		drive = new DifferentialDrive(lMotors, rMotors);
	}

	public void initDefaultCommand() {
		setDefaultCommand(new TeleDrive());
	}
	
	public void arcadeDrive(double moveValue, double rotateValue){
		drive.arcadeDrive(moveValue, -rotateValue, false);
	}
	
	public void arcadeDrive(double moveValue, double rotateValue, boolean squaredInputs){
		drive.arcadeDrive(moveValue, -rotateValue, squaredInputs);
	}
	
	public void curveatureDrive(double moveValue, double curveValue, boolean quickTurn) {
		drive.curvatureDrive(moveValue, curveValue, quickTurn);
	}
	
	public void tankDrive(double leftSpeed, double rightSpeed) {
		drive.tankDrive(leftSpeed, rightSpeed);
	}

	public void stop() {
		drive.stopMotor();
	}
	public double getAvgEncoderDistance() {
		return (lEncoder.getDistance() + rEncoder.getDistance()) / 2;
	}
	
	public void resetEncoders() {
		lEncoder.reset();
		rEncoder.reset(); 
	}
	
	public void resetGyro() {
		gyro.reset();
	}
	
	public Encoder getLEncoder() {
		return lEncoder;
	}
	
	public Encoder getREncoder() {
		return rEncoder;
	}
	
	public ADXRS450_Gyro getGyro() {
		return gyro;
	}
	
	public Ultrasonic getUltra() {
		return ultra;
	}
	
	public double getAngle() {
		return gyro.getAngle();
	}
}
