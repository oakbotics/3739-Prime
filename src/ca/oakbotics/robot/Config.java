package ca.oakbotics.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */

public class Config {

	// USB ports
	public final static int DRIVE_XBOX_PORT = 0;   	// Driver Xbox controller USB port
	public final static int OP_XBOX_PORT = 1;		// Operator Xbox controller USB port
	
	
	// Drive sensitivity percentage modifiers (eg. controller rotate value multiplied by 70%)
	public final static double THROTTLE_PERCENTAGE = 100, ROTATE_PERCENTAGE = 100;
	
	// PWM ports
	public final static int L_MTR_A = 1, L_MTR_B = 2, R_MTR_A = 3, R_MTR_B = 4;	// These motors are set as "inverted" to counteract how
	public final static boolean L_MTRS_INVRTD = false, R_MTRS_INVRTD = false; 	// the gear box by nature reverses the output from the motors
	
	public final static int L_INTAKE_MTR = 5, R_INTAKE_MTR = 7;
	public final static boolean L_INTAKE_INVRTD = false, R_INTAKE_INVRTD = true;
	
	public final static int WINCH_MTR_1 = 6, WINCH_MTR_2 = 8;
	public final static boolean WINCH_1_INVERTED = true, WINCH_2_INVERTED = true;

	
	public final static int L_ENCODER_A = 0, L_ENCODER_B = 1, R_ENCODER_A = 2, R_ENCODER_B = 3;
	public final static boolean L_ENCODER_INVRTD = false, R_ENCODER_INVRTD = true;
	public final static double ENC_CM_PER_TICK = 0.088;
										
	// Solenoid ports (DS = double-solenoid)
	public static final int ARM_DS_A = 1, ARM_DS_B = 0;
	
	// Sensor ports
	public static final int ULTRASONIC_SENS_PORT = 0;
	
	// Auto constants
	public static final double AUTO_SPEED_LIMIT = 0.4;	

}
