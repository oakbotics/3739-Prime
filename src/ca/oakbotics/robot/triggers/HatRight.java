package ca.oakbotics.robot.triggers;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.Trigger;

/**
 *
 */
public class HatRight extends Trigger {
	
	GenericHID controller;

	public HatRight(GenericHID controller) {
		this.controller = controller;
	}
	
    public boolean get() {
        return (controller.getPOV() == 90);
    }
}
