package ca.oakbotics.robot.triggers;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.buttons.Trigger;

/**
 *
 */
public class HatLeft extends Trigger {
	
	GenericHID controller;

	public HatLeft(GenericHID controller) {
		this.controller = controller;
	}
	
    public boolean get() {
        return (controller.getPOV() == 270);
    }
}
