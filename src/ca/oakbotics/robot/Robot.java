package ca.oakbotics.robot;

import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import ca.oakbotics.robot.OI;
import ca.oakbotics.robot.auto.AutoDrive;
import ca.oakbotics.robot.commands.AutoLeft;
import ca.oakbotics.robot.commands.AutoRight;
import ca.oakbotics.robot.commands.DriveToRange;
import ca.oakbotics.robot.commands.AutoCenter;
import ca.oakbotics.robot.subsystems.DriveBase;
import ca.oakbotics.robot.subsystems.Intake;
import ca.oakbotics.robot.subsystems.Winch;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {

	public static DriveBase driveBase;
	public static Intake intake;
	public static OI oi;
	public static PowerDistributionPanel pdp;
	public static Winch winch;
	
	public static String gameMessage;
		
	private SendableChooser<Integer> autoChooser;
	private Command autoCommand = null;

	@Override
	public void robotInit() {
		driveBase = new DriveBase();
		intake = new Intake();
		winch = new Winch();
		oi = new OI();
		pdp = new PowerDistributionPanel(1);
				
		autoChooser = new SendableChooser<Integer>();
		
		autoChooser.addObject("Left", 1);
		autoChooser.addDefault("Center", 2);
		autoChooser.addObject("Right", 3);
		autoChooser.addObject("BaseLine", 4);
		autoChooser.addObject("Ultrasonic", 5);
		
		SmartDashboard.putData("Mode Chooser", autoChooser);
	}

	/**
	 * This function is called once each time the robot enters Disabled mode.
	 * You can use it to reset any subsystem information you want to clear when
	 * the robot is disabled.
	 */
	@Override
	public void disabledInit() {
	}

	@Override
	public void disabledPeriodic() {
		gameMessage = DriverStation.getInstance().getGameSpecificMessage();
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select
	 * between different autonomous modes using the dashboard. The sendable
	 * chooser code works with the Java SmartDashboard. If you prefer the
	 * LabVIEW Dashboard, remove all of the chooser code and uncomment the
	 * getString code to get the auto name from the text box below the Gyro
	 *
	 * You can add additional auto modes by adding additional commands to the
	 * chooser code above (like the commented example) or additional comparisons
	 * to the switch structure below with additional strings & commands.
	 */
	@Override
	public void autonomousInit() {
		gameMessage = DriverStation.getInstance().getGameSpecificMessage();
		Integer autoMode = autoChooser.getSelected();
		
		if (autoMode == 1)
			autoCommand = new AutoLeft();
		else if (autoMode == 2)
			autoCommand = new AutoCenter();
		else if (autoMode == 3)
			autoCommand = new AutoRight();
		else if (autoMode == 4)
			autoCommand = new AutoDrive(100, 0.8);
		else if (autoMode == 5) 
			autoCommand = new DriveToRange(12, 0.5);
		autoCommand.start();
	}

	/**
	 * This function is called periodically during autonomous
	 */
	@Override
	public void autonomousPeriodic() {
		Scheduler.getInstance().run();
		
	}

	@Override
	public void teleopInit() {
		if (autoCommand != null) {
			autoCommand.cancel();
			System.out.println("Starting Telo auto over");
		}
		// This makes sure that the autonomous stops running when
		// teleop starts running. If you want the autonomous to
		// continue until interrupted by another command, remove
		// this line or comment it out.
	}

	/**
	 * This function is called periodically during operator control
	 */
	@Override
	public void teleopPeriodic() {
		Scheduler.getInstance().run();
	}

	/**
	 * This function is called periodically during test mode
	 */
	@Override
	public void testInit() {
		Robot.driveBase.getUltra().setAutomaticMode(true);
		Robot.driveBase.getUltra().setEnabled(true);
		Robot.driveBase.getLEncoder().reset();
		Robot.driveBase.getREncoder().reset();
	}
	
	@Override
	public void testPeriodic() {
		System.out.println("Ultrasonic Distance (Inches): " + Robot.driveBase.getUltra().getRangeInches());
		System.out.println("Gyro Angle (Degrees): " + Robot.driveBase.getGyro().getAngle());
		System.out.println("Left Encoder Distance(Inches): " + Robot.driveBase.getLEncoder().getDistance());
		System.out.println("Right Encoder Distance(Inches): " + Robot.driveBase.getREncoder().getDistance());
	}
}
