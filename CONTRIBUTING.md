# Branches

Just like last year, each individual feature will be a branch based from `master`.
However under my new authority `master` will remain much more protected to serve 
as the stable working base of our robot code.
 1. Every change to master must be commented thoroughly.
 2. Features must be added as merge requests from another branch.
 3. Changes to master must be tested on the production bot before being committed.
 4. The repository must not contain project configuration files specific to any IDE. Except for build config and eclipse config files.
 5. Robot code must not contain [magic numbers](https://stackoverflow.com/questions/47882/what-is-a-magic-number-and-why-is-it-bad).
 6. [Camel Case](https://google.github.io/styleguide/javaguide.html#s5-naming) and [spaces instead of tabs](https://stackoverflow.blog/2017/06/15/developers-use-spaces-make-money-use-tabs/).
 
 More to come...